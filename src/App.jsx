import React from 'react';
import TodoForm from './components/TodoForm';
import TodoList from './components/TodoList';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      todoList: [],
    };
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleFormSubmit(value) {
    this.setState((prevState) => {
      return {
        todoList: [...prevState.todoList, value],
      };
    });
  }

  render() {
    return (
      <div className="container">
        <h1>Awesome Todo App 🥳</h1>
        <TodoForm onSubmit={this.handleFormSubmit} />
        <TodoList list={this.state.todoList} />
      </div>
    );
  }
}

export default App;
