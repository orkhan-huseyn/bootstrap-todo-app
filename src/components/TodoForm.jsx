import React from 'react';

function TodoForm(props) {
  function handleSubmit(event) {
    event.preventDefault();
    const { todoInput } = event.target.elements;
    props.onSubmit(todoInput.value);
    todoInput.value = '';
  }

  return (
    <form onSubmit={handleSubmit} className="mb-3">
      <div className="mb-3">
        <label htmlFor="todoInput" className="form-label">
          Todo name
        </label>
        <input className="form-control" id="todoInput" />
      </div>
      <div className="d-grid">
        <button className="btn btn-primary">Submit</button>
      </div>
    </form>
  );
}

export default TodoForm;
