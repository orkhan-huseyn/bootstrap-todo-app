import React from 'react';
import TodoItem from './TodoItem';

function TodoList(props) {
  return (
    <ul className="list-group">
      {props.list.map((text) => (
        <TodoItem key={text} title={text} />
      ))}
    </ul>
  );
}

export default TodoList;
