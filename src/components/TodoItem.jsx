import React from 'react';

function TodoItem(props) {
  return (
    <li className="list-group-item d-flex justify-content-between align-items-start">
      <div className="me-auto">{props.title}</div>
      <button className="btn btn-sm btn-danger">Delete</button>
    </li>
  );
}

export default TodoItem;
